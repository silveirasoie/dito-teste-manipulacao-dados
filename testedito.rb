require 'json'
require 'uri'
require 'net/http'

require 'json'
require 'uri'
require 'net/http'

class ManipulacaoDados
    attr_accessor :eventosCompra, :eventosProduto, :resposta, :produtos

    def initialize
        caputurar
    end

    def timeline
        @resposta = {}
        @resposta[:timeline] = []
        manipular
        ordenar
        @resposta
    end

    private 

    def caputurar
        url = URI("https://storage.googleapis.com/dito-questions/events.json")
        http = Net::HTTP.new(url.host)
        request = Net::HTTP::Get.new(url)
        response = http.request(request)
        dados = response.read_body.force_encoding("utf-8")
        dados = JSON.parse(dados)

        @eventosCompra = dados['events'].select do |hash|
            hash['event'] === 'comprou'
        end
        @eventosProduto = dados['events'].select do |hash|
            hash['event'] === 'comprou-produto'
        end
        @produtos = []
        @eventosProduto.each do |produto|
            atributos = {} 
            produto['custom_data'].each do |atributo|
                atributos[get_key_produto(atributo['key']).to_sym] = atributo['value']
            end
            @produtos.push(atributos)
        end
    end

    def get_key_produto(key)
        valores = { 'product_name' => 'name', 'product_price' => 'price', 'transaction_id' => 'transaction_id' }
        valores[key]
    end

    def manipular
        @eventosCompra.each do |dadoCompra|
            compra = {}
            compra[:timestamp] = dadoCompra['timestamp']
            compra[:store_name] = get_value_compra( dadoCompra['custom_data'], 'store_name' )
            compra[:transaction_id] = get_value_compra( dadoCompra['custom_data'], 'transaction_id' )
            compra[:products] = get_produtos( compra[:transaction_id] )
            compra[:revenue] = calcular_revenue( compra[:products] )
            @resposta[:timeline].push( compra )
        end
    end

    def ordenar
       @resposta[:timeline].sort_by! { |hsh| hsh[:timestamp] }.reverse!
    end

    def get_value_compra( custom_data, field )
        return custom_data.select do |hash|
            hash['key'] === field
        end.first['value']
    end
    

    def get_produtos( transaction_id )
        result = @produtos.select do |produto|
            produto[:transaction_id] === transaction_id
        end
        result ||= []
        return_keys = ['name','price']
        result.each { |h| h.delete(:transaction_id) }
    end

    def calcular_revenue(produtos)
        produtos.inject(0) {|sum, hash| sum + hash[:price]}
    end
end


manipulacao_dados = ManipulacaoDados.new
manipulacao_dados.timeline

